def create_tables(c):
    # Airplanes table
    c.execute('''CREATE TABLE IF NOT EXISTS airplanes (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    status TEXT,
                    number INTEGER,
                    positionX REAL,
                    positionY REAL,
                    height REAL,
                    speed INTEGER,
                    angle INTEGER,
                    fuel REAL)''')
def insert_new_airplane(conn, c, airplane):
    with conn:
        c.execute('''INSERT INTO airplanes (status, number, positionX, positionY, height, speed, angle, fuel)
                     VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
                  ("created", airplane.number, airplane.posX, airplane.posY, airplane.height, airplane.speed,
                   airplane.angle, airplane.fuel))

def collision_airplane(conn, c, airplane):
    with conn:
        c.execute('''INSERT INTO airplanes (status, number, positionX, positionY, height, speed, angle, fuel)
                     VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
                  ("destroyed", airplane.number, airplane.posX, airplane.posY, airplane.height, airplane.speed,
                   airplane.angle, airplane.fuel))
def landed_airplane(conn, c, airplane):
    with conn:
        c.execute('''INSERT INTO airplanes (status, number, positionX, positionY, height, speed, angle, fuel)
                     VALUES (?, ?, ?, ?, ?, ?, ?, ?)''',
                  ("landed", airplane.number, airplane.posX, airplane.posY, airplane.height, airplane.speed,
                   airplane.angle, airplane.fuel))

