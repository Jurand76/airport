# airport.py
import socket
import json, math
from configure import landing_zones
from configure import landing_stripes
import configure
import threading

class HeightManager:
    def __init__(self, start_height=1000, end_height=5000, step=40):
        self.available_heights = list(range(start_height, end_height + 1, step))
        self.used_heights = set()

    def getHeight(self):
        if self.available_heights:
            height = self.available_heights.pop(0)  # Take the first available height
            self.used_heights.add(height)
            return height
        else:
            return -1

    def releaseHeight(self):
        if self.used_heights:
            height = self.used_heights.pop()  # Take the first used height
            self.available_heights.append(height)  # And make it available again

class Airport:
    running = True
    def getCurrentZone(self, landing_zone_id):
        return landing_zones.get(landing_zone_id)

    def getNewZone(self, landing_zone_id):
        if landing_zone_id == -1:
            response = landing_zones.get(-1)
        if landing_zone_id == 1:
            response = landing_zones.get(2)
        if landing_zone_id == 2:
            response = landing_zones.get(3)
        if landing_zone_id == 3:
            response = landing_zones.get(4)
        if landing_zone_id == 4:
            response = landing_zones.get(1)
        if landing_zone_id == 11:
            response = landing_zones.get(12)
        if landing_zone_id == 12:
            response = landing_zones.get(13)
        if landing_zone_id == 13:
            response = landing_zones.get(31)
        if landing_zone_id == 21:
            response = landing_zones.get(22)
        if landing_zone_id == 22:
            response = landing_zones.get(23)
        if landing_zone_id == 23:
            response = landing_zones.get(32)
        return response


    def checkIfIsLandingPossible(self, x, y):
        if landing_stripes[1]['free'] == True and landing_stripes[2]['free'] == True:
            dist1 = math.sqrt((x - landing_stripes[1]['x'])**2 + (y - landing_stripes[1]['y'])**2)
            dist2 = math.sqrt((x - landing_stripes[2]['x'])**2 + (y - landing_stripes[2]['y'])**2)
            if dist1 > dist2:
                return 2
            else:
                return 1

        if landing_stripes[1]['free'] == True:
            return 1

        if landing_stripes[2]['free'] == True:
            return 2

        return -1

    def firstTargetCalculator(self, x, y):
        minDistance = 10000
        bestZone = 1
        for i in range(1, 5):
            distanceFromWaitingPoints = math.sqrt(
                (x - landing_zones[i]['x']) ** 2 + (y - landing_zones[i]['y']) ** 2)
            if distanceFromWaitingPoints < minDistance:
                bestZone = i
                minDistance = distanceFromWaitingPoints

        return landing_zones[bestZone]

    def close_airport(self):
        self.running = False

    def handle_airplane(self, connection, client_address, heightManager):
        try:
            #print('connection from', client_address)

            while self.running:
                data = connection.recv(1024)
                if data:
                    # Decode data to JSON
                    message = json.loads(data.decode())
                    #print(f'Received message: {message}')

                    if message == 'getNewHeight':
                        response = {"newHeight": heightManager.getHeight()}

                    if 'newFirstTarget' in message:
                        response = {"newFirstTarget": self.firstTargetCalculator(message['x'], message['y'])}

                    if 'currentZone' in message:
                        landing_zone_id = int(message['currentZone'])
                        response = {"currentZone": self.getCurrentZone(landing_zone_id)}

                    if 'newZone' in message:
                        landing_zone_id = int(message['newZone'])
                        response = self.getNewZone(landing_zone_id)

                    if 'targetReached' in message:
                        reached_zone_id = message['reachedZone']
                        newZone = self.getNewZone(reached_zone_id)
                        response = newZone

                    if 'isLandingPossible' in message:
                        freeLandingStripe = self.checkIfIsLandingPossible(message['x'], message['y'])
                        if freeLandingStripe > 0:
                            landing_stripes[freeLandingStripe]['free'] = False
                            landing_stripes[freeLandingStripe]['airplane'] = int(message['airplane'])
                            #print(f"Free landing stripe: {freeLandingStripe}")
                            response = self.getCurrentZone(landing_stripes[freeLandingStripe]['zone'])
                        else:
                            response = self.getCurrentZone(landing_stripes[0]['zone'])

                    if 'releaseLandingStripe' in message:
                        numberOfLandingStripe = message['stripe']
                        landing_stripes[numberOfLandingStripe]['free'] = True
                        #print(f"Released landing stripe: {numberOfLandingStripe}")
                        response = True

                    if 'closeAirport' in message:
                        response = True
                        connection.sendall(json.dumps(response).encode())
                        connection.close()
                        self.running = False
                        break

                    if 'closeConnectionWithAirplane' in message:
                        response = True
                        connection.sendall(json.dumps(response).encode())
                        connection.close()
                        break

                    # Send response back to the client
                    if connection:
                        connection.sendall(json.dumps(response).encode())
                else:
                    continue
        except Exception as e:
            print(f"Error occured with client {client_address}: {e}")

        finally:
            connection.close()
            print(f"Connection closed with {client_address}")


    def open_airport(self):
        # Create HeightManager object

        heightManager = HeightManager()

        # Create a TCP/IP socket
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Bind the socket to the address and port
        server_address = (configure.HOST, configure.PORT)
        server_socket.bind(server_address)

        # Listen for incoming connections
        server_socket.listen(1)

        while self.running:
            # Wait for a connection

            connection, client_address = server_socket.accept()
            client_thread = threading.Thread(target=self.handle_airplane,
                                             args=(connection, client_address, heightManager))
            client_thread.daemon = True
            client_thread.start()

        server_socket.close()
        print("Server has been closed")
