import math
import configure
def checkCollisions(airplanes):

    for i in range(len(airplanes)):
        airplane_to_check = airplanes[i]
        posX1 = airplane_to_check.posX
        posY1 = airplane_to_check.posY
        height1 = airplane_to_check.height

        for j in range(i+1, len(airplanes)):
            second_airplane = airplanes[j]
            if second_airplane.number != airplane_to_check.number:
                posX2 = second_airplane.posX
                posY2 = second_airplane.posY
                height2 = second_airplane.height

                dist2D = math.sqrt((posX2 - posX1)**2 + (posY2 - posY1)**2)
                if (dist2D < configure.COLLISION_DISTANCE*30) and (abs(height2-height1) < 45):
                    if 90 < abs(airplane_to_check.angle - second_airplane.angle) <= 180:
                        airplane_to_check.collisionCorrection = 10
                        second_airplane.collisionCorrection = 7
                    else:
                        if airplane_to_check.collisionCorrection == 0:
                            if airplane_to_check.angle > second_airplane.angle:
                                airplane_to_check.collisionCorrection = 8
                                second_airplane.collisionCorrection = -2
                            else:
                                airplane_to_check.collisionCorrection = -2
                                second_airplane.collisionCorrection = 8
                else:
                    if (dist2D < configure.COLLISION_DISTANCE * 15) and (abs(height2 - height1) < 45):
                        airplane_to_check.collisionCorrection = 2*airplane_to_check.collisionCorrection

                    else:
                        if airplane_to_check.collisionCorrection != 0 and second_airplane.collisionCorrection != 0:
                            airplane_to_check.collisionCorrection = 0
                            second_airplane.collisionCorrection = 0


                if (dist2D < configure.COLLISION_DISTANCE) and (abs(height2-height1) < configure.COLLISION_DISTANCE):
                    print(f"Collision between airplanes: {airplane_to_check.number} and {second_airplane.number}")
                    airplane_to_check.collision = True
                    second_airplane.collision = True
                    second_airplane.landingZone = 40
                    airplane_to_check.landingZone = 40
