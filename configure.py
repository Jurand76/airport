HOST = 'localhost'              # connection to server parameters
PORT = 10000

AIRPLANE_SIZE = 100             # in pixels
CLOCK_TICK = 360                # 60 - normal speed 60fps
SCREEN_WIDTH = 1000             # in pixels
SCREEN_HEIGHT = 1000            # in pixels
FUEL_TANK = 10800               # in seconds of flight
COLLISION_DISTANCE = 20         # in meters

# configuring landing zones and landing stripes
# you can change 'x' parameter in 1 and 2 landing stripes to move stripe
# the same parameter 'x' should be in landing zones numbers from 11 to 23
# landing stripe 1 - zones 11, 12, 13   landing stripe 2 - zones 21, 22, 23

landing_zones = {
    -1: {'id': -1, 'x': -1, 'y': -1, 'h': -1, 's': -1, 'd': -1},
    1: {'id': 1, 'x': 1000, 'y': 1000, 'h': -1, 's': 300, 'd': 1200},
    2: {'id': 2, 'x': 1000, 'y': 9000, 'h': -1, 's': 300, 'd': 1200},
    3: {'id': 3, 'x': 9000, 'y': 9000, 'h': -1, 's': 300, 'd': 1200},
    4: {'id': 4, 'x': 9000, 'y': 1000, 'h': -1, 's': 300, 'd': 1200},
    11: {'id': 11, 'x': 3000, 'y': 7000, 'h': 700, 's': 250, 'd': 300},
    12: {'id': 12, 'x': 3000, 'y': 3000, 'h': 300, 's': 200, 'd': 100},
    13: {'id': 13, 'x': 3000, 'y': 200, 'h': 0, 's': 60, 'd': 100},
    21: {'id': 21, 'x': 7000, 'y': 7000, 'h': 700, 's': 250, 'd': 300},
    22: {'id': 22, 'x': 7000, 'y': 3000, 'h': 300, 's': 200, 'd': 100},
    23: {'id': 23, 'x': 7000, 'y': 200, 'h': 0, 's': 60, 'd': 100},
    31: {'id': 31, 'x': -1, 'y': -1, 'h': -1, 's': 0, 'd': 100},
    32: {'id': 32, 'x': -1, 'y': -1, 'h': -1, 's': 0, 'd': 100},
    40: {'id': 40, 'x': -1, 'y': -1, 'h': -1, 's': 0, 'd': 100},
}

landing_stripes = {
    0: {'id': 0, 'free': False, 'x': -1, 'y': -1, 'zone': -1},
    1: {'id': 1, 'free': True, 'x': 3000, 'y': 7000, 'zone': 11},
    2: {'id': 2, 'free': True, 'x': 7000, 'y': 7000, 'zone': 21}
}
