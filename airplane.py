import pygame
from pygame.locals import QUIT
import math
from simple_pid import PID
import configure
import socket, json

class AirPlane:
    def __init__(self, number, posx, posy, height, angle, speed, fuel, target, minDistanceFromTarget, targetReached,
                 landingZone, landed, stopped):
        self.number = number
        self.image = pygame.image.load("plane.png").convert_alpha()
        self.imageCollision = pygame.image.load("collision.png")
        self.posX = posx
        self.posY = posy
        self.height = height
        self.angle = angle
        self.speed = speed
        self.fuel = fuel
        self.scale = int((self.height / 100 + 50) / 100 * configure.AIRPLANE_SIZE)
        self.rotateSpeed = 0
        self.last_fuel_time = pygame.time.get_ticks()
        self.is_active = True
        self.destinationHeight = height
        self.destinationSpeed = speed
        self.collision = False
        self.update_image()
        self.targetX = target[0]
        self.targetY = target[1]
        self.targetReached = targetReached
        self.minDistanceFromTarget = minDistanceFromTarget
        self.landingZone = landingZone
        self.landed = landed
        self.stopped = stopped
        self.collisionCorrection = 0
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((configure.HOST, configure.PORT))
        self.getNewHeight()
        self.getFirstTarget(self.posX, self.posY)


    def getResponseFromAirport(self, message):
        self.client_socket.sendall(json.dumps(message).encode())
        response = json.loads(self.client_socket.recv(1024).decode())
        #self.client_socket.close()
        return response

    def getFirstTarget(self, x, y):
        message = {
            "newFirstTarget": True,
            "x": self.posX,
            "y": self.posY
        }
        response = self.getResponseFromAirport(message)
        newFirstTarget = response['newFirstTarget']
        #print(f"New target received {newFirstTarget}")
        self.configureTarget(newFirstTarget['x'], newFirstTarget['y'], newFirstTarget['d'])
        self.changeSpeed(newFirstTarget['s'])
        self.landingZone = newFirstTarget['id']

    def getNewHeight(self):
        message = "getNewHeight"
        response = self.getResponseFromAirport(message)
        newHeight = response['newHeight']
        #print(f"New height received {newHeight}")
        self.changeHeight(newHeight)

    def update_image(self):
        self.scale = int((self.height / 62.5 + 20) / 100 * configure.AIRPLANE_SIZE)
        scaled_image = pygame.transform.scale(self.image,
                                              (int(self.scale), int(self.scale)))
        self.rotated_image = pygame.transform.rotate(scaled_image, -self.angle)
        self.rect = self.rotated_image.get_rect(center=(self.posX / 10, self.posY / 10))

    def move(self):
        if math.fabs(self.destinationHeight - self.height) > 1:
            if self.destinationHeight > self.height:
                self.height += 1
            if self.destinationHeight < self.height:
                self.height -= 1
        else:
            self.height = self.destinationHeight

        if math.fabs(self.destinationSpeed - self.speed) > 3:
            if self.destinationSpeed > self.speed:
                self.speed += 3
            if self.destinationSpeed < self.speed:
                self.speed -= 3
        else:
            self.speed = self.destinationSpeed

        self.angle += self.rotateSpeed / 100
        if self.angle >= 360:
            self.angle = self.angle - 360
        if self.angle < 0:
            self.angle += 360

        self.speed -= self.collisionCorrection / 3

        self.posX += math.sin(math.radians(self.angle)) * self.speed / 100
        self.posY += -math.cos(math.radians(self.angle)) * self.speed / 100

        self.changeAngle()
        self.didReachedTarget()
        self.checkFuel()

        if self.speed < 1:
            self.stopped = True

        if self.stopped:
            self.planeStopped()

    def draw(self, screen):
        if not self.collision:
            screen.blit(self.rotated_image, self.rect.topleft)
            if self.collisionCorrection != 0:
                pygame.draw.circle(screen, (255, 0, 0), (self.posX / 10, self.posY / 10), 40, 2)
        else:
            screen.blit(self.imageCollision, self.rect.topleft)
        self.move()
        self.update_image()

    def getDistanceFromTarget(self):
        distanceX = self.posX - self.targetX
        distanceY = self.posY - self.targetY
        distance = math.sqrt(distanceX * distanceX + distanceY * distanceY)
        return distance

    def isLandingPossible(self):
        message = {
            "isLandingPossible": True,
            "airplane": self.number,
            "x": self.posX,
            "y": self.posY
        }
        response = self.getResponseFromAirport(message)
        return response

    def planeStopped(self):
        message = {
            "closeConnectionWithAirplane": True
        }
        response = self.getResponseFromAirport(message)

    def lastPlaneLanded(self):
        message = {
            "closeAirport": True
        }
        response = self.getResponseFromAirport(message)

    def releaseLandingStripe(self, stripe, number):
        message = {
            "releaseLandingStripe": True,
            "airplane": number,
            "stripe": stripe
        }
        response = self.getResponseFromAirport(message)
        return response


    def didReachedTarget(self):
        dist = self.getDistanceFromTarget()
        if dist < self.minDistanceFromTarget and abs(self.height - self.destinationHeight) < 20:
            self.targetReached = True

            if self.landingZone < 10:
                freeLandingStripe = self.isLandingPossible()
                if freeLandingStripe['id'] <= 0:
                    message = {
                        "targetReached": True,
                        "reachedZone": self.landingZone
                    }
                    newZone = self.getResponseFromAirport(message)
                    #print(f"New zone: {newZone}")
                    self.landingZone = newZone['id']
                    if newZone['s'] != -1:
                        self.destinationSpeed = newZone['s']
                    if newZone['h'] != -1:
                        self.destinationHeight = newZone['h']
                    self.configureTarget(newZone['x'], newZone['y'], newZone['d'])
                else:
                    self.configureTarget(freeLandingStripe['x'], freeLandingStripe['y'], freeLandingStripe['d'])
                    self.changeSpeed(freeLandingStripe['s'])
                    self.landingZone = freeLandingStripe['id']
                    self.changeHeight(freeLandingStripe['h'])
            else:
                message = {
                    "targetReached": True,
                    "reachedZone": self.landingZone
                }
                newZone = self.getResponseFromAirport(message)
                #print(f"New zone: {newZone}")
                self.landingZone = newZone['id']
                if newZone['s'] != -1:
                    self.destinationSpeed = newZone['s']
                if newZone['h'] != -1:
                    self.destinationHeight = newZone['h']
                self.configureTarget(newZone['x'], newZone['y'], newZone['d'])

                if self.landingZone == 23 or self.landingZone == 13:
                    landing = self.releaseLandingStripe(int(self.landingZone / 10), self.number)
                    if landing:
                        self.landed = True
                    else:
                        pass
                        #print(f"Problems with landing airplane {self.number}")

    def getAngleDifference(self, plane, dest):
        diff = 0
        angle_1 = plane
        angle_2 = dest
        if -180 < dest < 0:
            angle_2 = 360 + dest
            angle_1 = 360 + plane

        diff = (angle_2 - angle_1 + 180) % 360 - 180
        return -diff

    def changeAngle(self):
        if self.targetX >= 0:
            destination_angle = math.degrees(
                math.atan2((self.targetX - self.posX), -(self.targetY - self.posY)))

            angleToZero = self.getAngleDifference(self.angle, destination_angle)
            pid_a = PID(0.4, 0, 0.2, setpoint=0)
            value_a = pid_a(angleToZero)
            self.rotateSpeedChange(value_a)

    def configureTarget(self, x, y, distance):
        self.targetX = x
        self.targetY = y
        self.targetReached = False
        self.minDistanceFromTarget = distance

    def rotateSpeedChange(self, amount):
        newSpeed = amount
        if newSpeed < -20:
            newSpeed = -20
        if newSpeed > 20:
            newSpeed = 20

        newSpeed = newSpeed + self.collisionCorrection
        self.rotateSpeed = newSpeed

    def rotateSpeedZero(self):
        self.rotateSpeed = 0

    def getPosition(self):
        return self.posX, self.posY, self.height, self.angle

    def changeHeight(self, destination):
        self.destinationHeight = destination

    def changeSpeed(self, destination):
        self.destinationSpeed = destination

    def drawCollision(self):
        self.speed = 0
        self.collision = True
        self.is_active = False
        self.landingZone = 40

    def checkFuel(self):
        current_time = pygame.time.get_ticks()
        self.fuel = self.fuel - ((current_time - self.last_fuel_time) / 1000) * configure.CLOCK_TICK / 60
        self.last_fuel_time = current_time
        if self.fuel <= 0:
            self.fuel = 0
            #print('Out of fuel')
            self.drawCollision()
        return self.fuel

    def printParameters(self):
        print(
            f"{self.targetReached} "
            f"speed: {self.speed} "
            f"x: {self.posX:.2f} y"
            f": {self.posY:.2f} "
            f"dyst: {self.getDistanceFromTarget():.2f} "
            f"kąt: {self.angle:.2f} "
            f"obecna wysokość: {self.height:.2f} ")
