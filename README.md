# Airport

Konfiguracja - plik configure.py
Uruchomienie - main.py
Wyswietlenie zawartości bazy - view_database.py

Wygenerowanie samolotu - naciśnij SPACJĘ

Zakończenie programu - po wylądowaniu wszystkich samolotów

-----------------------------------
Projekt Lotnisko

Masz stworzyć system automatycznego lądowania na lotnisku.

Założenia techniczne.

1. Klient / sewer pracujący na socketach.

2. Informacje o nawiązanych połączeniach, kolizjach i udanych lądowaniach zapisujesz w bazie.

3. Klienty to samoloty

4. Serwer to system automatycznego lądowania.

5.Założenia biznesowe

- Lotnisko posiada 2 pasy
- W przestrzeni powietrznej lotniska może się znajdować maksymalnie 100 samolotów, każdy kolejny, w trakcie próby połączenia powinien otrzymać informację o konieczności znalezienia sobie innego lotniska i znika z twojego "radaru"
- Lotnisko zawiaduje przestrzenią 10 km na 10 km i wysokości 5 km.
- Każdy przylatujący samolot ma paliwa na 3 godziny lotu
- Należy ustalić korytarze powietrzne służące do lądowania
- Samoloty pojawiają się w losowym miejscu na granicy obszaru powietrznego, POZA korytarzami powietrznymi na wysokości 2km do 5km
- Sprowadzenie samolotu na ziemię polega na skierowaniu go do korytarza powietrznego i pilnowania jego czystej drogi do pasa startowego, na pasie startowym samolot ma mieć wysokość równą 0m inaczej lądowanie jest uznane za nieudane i następuje kolizja
- Jeśli dwa samoloty znajdą się w odległości 10m od siebie, uznajemy, że nastąpiła kolizja
- Jeśli w samolocie podczas oczekiwania skończy się paliwo, uznajemy, że nastąpiła kolizja


Twoim zadaniem jest stworzenie systemu, który bezpiecznie będzie sprowadzał wszystkie samoloty na ziemię