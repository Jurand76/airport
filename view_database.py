import sqlite3

# Connect to the SQLite database
conn = sqlite3.connect('airplanes_simulation.db')
cursor = conn.cursor()

# SQL query to select all records from the airplanes table
query = "SELECT status, number, positionX, positionY, height, speed, angle, fuel FROM airplanes"

try:
    cursor.execute(query)
    records = cursor.fetchall()

    print(
        f"{'Number':<10} {'Status':<10} {'PosX':<10} {'PosY':<10} {'Height':<10} {'Speed':<10} {'Angle':<10} {'Fuel':<10}")

    for row in records:
        status, number, posX, posY, height, speed, angle, fuel = row
        print(
            f"{number:<10} {status:<10} {posX:<10.0f} {posY:<10.0f} {height:<10.0f} {speed:<10.0f} {angle:<10.0f} {fuel:<10.2f}")


except sqlite3.Error as e:
    print("Database error:", e)

finally:
    # Close the cursor and connection
    cursor.close()
    conn.close()