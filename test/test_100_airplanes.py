import pygame
from pygame.locals import QUIT, KEYDOWN, K_SPACE, K_ESCAPE
from airplane import AirPlane
import configure
import random
from configure import landing_zones
from configure import landing_stripes
from collisions import checkCollisions
from airport import Airport
import threading
import sqlite3
import database
import os

# Initialize Pygame
pygame.init()

# Initialize sq3lite database
conn = sqlite3.connect('test_100_airplanes.db')
c = conn.cursor()
database.create_tables(c)

# Initialize Airport
port = Airport()
airport_thread = threading.Thread(target=port.open_airport)
airport_thread.start()

# Colors for landing stripes
red = (255, 0, 0)
green = (0, 200, 0)
gray = (100, 100, 100)
light_gray = (190, 190, 190)

# Screen and clock
screen = pygame.display.set_mode((configure.SCREEN_WIDTH, configure.SCREEN_WIDTH))
pygame.display.set_caption('Animated Airplanes')
clock = pygame.time.Clock()

# Create airplanes list
airplanes = []
airplane_next = 1
airplanes_lock = threading.Lock()

airplanes_total = 0
airplanes_landed = 0
airplanes_destroyed = 0

def update_console():
    global airplanes_total, airplanes_landed, airplanes_destroyed
    os.system('cls')
    print(f"Total count of created airplanes:   {airplanes_total}")
    print(f"Total count of landed airplanes:    {airplanes_landed}")
    print(f"Total count of destroyed airplanes: {airplanes_destroyed}")


def add_new_airplane(airplane_next):
    if len(airplanes) < 100:
        side = random.randint(0, 1)
        placeY = random.randint(0, 10000)
        newSpeed = random.randint(400, 1200)
        newHeight = random.randint(2000, 5000)
        newAngle = random.randint(0, 180)

        if side == 0:
            new_airplane = AirPlane(airplane_next, 0, placeY, newHeight, newAngle, newSpeed, configure.FUEL_TANK,
                                (landing_zones[1]['x'], landing_zones[1]['y']), 1200,
                                False, -1, False, False)

        if side == 1:
            new_airplane = AirPlane(airplane_next, 10000, placeY, newHeight, 360 - newAngle, newSpeed, configure.FUEL_TANK,
                                (landing_zones[1]['x'], landing_zones[1]['y']), 1200, False, -1, False, False)

        with airplanes_lock:
            airplanes.append(new_airplane)
            database.insert_new_airplane(conn, c, new_airplane)


running = True
ADD_PLANE_EVENT = pygame.USEREVENT + 1
pygame.time.set_timer(ADD_PLANE_EVENT, 5000)

while running:
    screen.fill((0, 0, 0))
    pygame.draw.line(screen, green, (landing_stripes[1]['x'] / 10 - 30, 0), (landing_stripes[1]['x'] / 10 - 30, 300), 2)
    pygame.draw.line(screen, green, (landing_stripes[1]['x'] / 10 + 30, 0), (landing_stripes[1]['x'] / 10 + 30, 300), 2)
    pygame.draw.rect(screen, gray, (landing_stripes[1]['x'] / 10 - 29, 0, 59, 301))

    pygame.draw.line(screen, green, (landing_stripes[2]['x'] / 10 - 30, 0), (landing_stripes[2]['x'] / 10 - 30, 300), 2)
    pygame.draw.line(screen, green, (landing_stripes[2]['x'] / 10 + 30, 0), (landing_stripes[2]['x'] / 10 + 30, 300), 2)
    pygame.draw.rect(screen, gray, (landing_stripes[2]['x'] / 10 - 29, 0, 59, 301))

    x1, y1 = (landing_stripes[1]['x'] / 10, 0)
    x2, y2 = (landing_stripes[1]['x'] / 10, 300)
    dx = x2 - x1
    dy = y2 - y1
    num_dashes = int(dy / 10)

    for i in range(num_dashes):
        if i % 2 == 0:
            pygame.draw.line(screen, light_gray, (x1, y1 + i * 10), (x1, y1 + (i + 1) * 10))
            pygame.draw.line(screen, light_gray, (x1 + 400, y1 + i * 10), (x1 + 400, y1 + (i + 1) * 10))

    # Event handling
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
        elif event.type == ADD_PLANE_EVENT:
            if airplane_next <= 100:
                threading.Thread(target=add_new_airplane(airplane_next)).start()
                airplane_next += 1
                airplanes_total += 1
                update_console()
        elif event.type == KEYDOWN:  # add new airplane after clicking SPACE BAR
            if event.key == K_ESCAPE:
                running = False
                conn.close()            # stopping sq3lite database
                port.close_airport()


    checkCollisions(airplanes)

    for airplane in airplanes:

        if airplane.is_active:
            position = airplane.getPosition()
            current_height = position[2]

            if current_height <= 0.1 and not airplane.landed:
                landed = True
                airplane.landed = True

            with airplanes_lock:
                airplane.draw(screen)

            if airplane.landingZone == 31:
                print(
                    f"Airplane: {airplane.number} landed and saved to database with {int(100 * airplane.fuel / configure.FUEL_TANK)}% of fuel tank")
                database.landed_airplane(conn, c, airplane)
                airplanes_landed += 1
                airplane.planeStopped()
                update_console()
                airplanes.remove(airplane)
                if airplanes_total > 0 and len(airplanes) == 0:
                    print("Last plane landed!")
                    running = False
                    conn.close()
                    port.close_airport()

            if airplane.landingZone == 32:
                print(
                    f"Airplane: {airplane.number} landed and saved to database with {int(100 * airplane.fuel / configure.FUEL_TANK)}% of fuel tank")
                database.landed_airplane(conn, c, airplane)
                airplanes_landed += 1
                airplane.planeStopped()
                update_console()
                airplanes.remove(airplane)
                if airplanes_total > 0 and len(airplanes) == 0:
                    print("Last plane landed!")
                    running = False
                    conn.close()
                    port.close_airport()

            if airplane.landingZone == 40:
                print(f"Airplane: {airplane.number} was destroyed !!!")
                database.collision_airplane(conn, c, airplane)
                airplanes_destroyed += 1
                airplane.planeStopped()
                update_console()
                airplanes.remove(airplane)
                if airplanes_total > 0 and len(airplanes) == 0:
                    print("Last plane destroyed!")
                    running = False
                    conn.close()
                    port.close_airport()

    pygame.display.flip()
    clock.tick(configure.CLOCK_TICK)

pygame.quit()
